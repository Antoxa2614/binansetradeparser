module gitlab.com/antoxa2614/binansetradeparser

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.10
	github.com/joho/godotenv v1.5.1
	gitlab.com/golight/boilerplate v0.0.12
	gitlab.com/golight/orm v0.0.5
	go.uber.org/zap v1.24.0
	golang.org/x/sync v0.3.0
)

require (
	github.com/Masterminds/squirrel v1.5.4 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/quicktemplate v1.7.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/grpc v1.39.0-dev // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
