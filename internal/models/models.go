package models

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index
type HistoricalTradeDTO struct {
	ID           int64   `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name         string  `json:"name" db:"name" db_type:"varchar(255)" db_default:"default null" db_ops:"create,update"`
	Price        string  `json:"price" db:"price" db_type:"varchar(255)" db_default:"default null" db_ops:"create,update"`
	Qty          string  `json:"qty" db:"qty" db_type:"varchar(255)" db_default:"default null" db_ops:"create,update"`
	QuoteQty     string  `json:"quote_qty" db:"quote_qty" db_type:"varchar(255)" db_default:"default null" db_ops:"create,update"`
	Time         float64 `json:"open" db:"open" db_type:"decimal" db_default:"default 0" db_ops:"create,update"`
	IsBuyerMaker bool    `json:"is_buyer_maker" db:"is_buyer_maker" db_ops:"" db_type:"bool" db_default:"" db_index:""`
	IsBestMatch  bool    `json:"is_best_match" db:"is_best_match" db_ops:"" db_type:"bool" db_default:"" db_index:""`
}

type HistoricalTrade struct {
	ID           int64   `json:"id"`
	Name         string  `json:"name"`           // Название торговой пары
	Price        string  `json:"price"`          // Цена по которой была совершена сделка
	Qty          string  `json:"qty"`            // Количество базового актива, которое было продано или куплено в рамках сделки
	QuoteQty     string  `json:"quote_qty"`      // Общая стоимость сделки в котируемой валюте, рассчитываемая как произведение цены и количества.
	Time         float64 `json:"time"`           // Временная метка (timestamp) сделки, выраженная в миллисекундах, когда сделка была выполнена.
	IsBuyerMaker bool    `json:"is_buyer_maker"` // Булево значение, указывающее, является ли инициатор сделки покупателем (true) или продавцом (false).
	IsBestMatch  bool    `json:"is_best_match"`  // Булево значение, указывающее, является ли сделка "лучшим сопоставлением" (best match),
	// что означает, что сделка произошла по наилучшей доступной цене на рынке.
}

type HistoricalTrades []HistoricalTrade

func (H *HistoricalTradeDTO) TableName() string {
	return "trades"
}
func (H *HistoricalTradeDTO) OnCreate() []string {
	return []string{}
}

// структура для информации о торгуемых парах
type ExchangeInfo struct {
	Symbols []SymbolInfo `json:"symbols"`
}

type SymbolInfo struct {
	Symbol string `json:"symbol"`
	Status string `json:"status"`
}
