package worker

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/antoxa2614/binansetradeparser/internal/models"
	"gitlab.com/antoxa2614/binansetradeparser/internal/storage"
	"go.uber.org/zap"
	"net/http"
)

// Основные константы для запроса к API Binance
const (
	BinanceAPIURL      = "https://api.binance.com/api/v3/historicalTrades"
	ExchangeInfoAPIURL = "https://api.binance.com/api/v1/exchangeInfo"
)

type Worker struct {
	logger    *zap.Logger
	ApiKey    string
	IsRunning bool
	StopChan  chan bool
	Storage   *storage.Storage
}

func NewWorker(logger *zap.Logger, storage *storage.Storage, apiKey string) *Worker {
	return &Worker{logger: logger, Storage: storage, ApiKey: apiKey}
}

func (w *Worker) Run(ctx context.Context, stopChan chan bool) error {
	w.logger.Info("worker started")

	// получение всех пар торгов
	pairs, err := w.fetchAllPairs()
	if err != nil {
		w.logger.Error("fetchAllPairs error", zap.Error(err))
		return err
	}
	// перебираем пары, для каждой парсим историю сделок
	for _, pair := range pairs {
		// механизм остановки парсинга
		select {
		case <-stopChan:
			w.logger.Info("Stop parsing")
			return nil
			// если нет сигнала из канала, код продолжает выполнение
		default:
		}

		// получение истории сделок  для пары
		trades, err := w.fetchHistoricalTrades(pair)
		if err != nil {
			w.logger.Error("get api error", zap.Error(err))
			continue
		}
		// данные выбраны, переходим к следующей паре
		if len(trades) == 0 {
			continue
		}
		// сохранение истории сделок в репозитории
		err = w.Storage.Save(ctx, trades)
		if err != nil {
			w.logger.Error("update error", zap.Error(err))
		}
	}
	w.logger.Info("worker finished")
	return nil
}

// тип для сырых данных (массив массивов) с Binance API

// получаем историю сделок для заданной пары
func (w *Worker) fetchHistoricalTrades(pair string) ([]models.HistoricalTrade, error) {
	// формирование URL запроса
	url := fmt.Sprintf("%s?symbol=%s&limit=1000", BinanceAPIURL, pair)

	Trades := make(models.HistoricalTrades, 0) // сырые данные сделок

	// делаем запрос к Binance API
	if err := w.makeAPIRequest(url, &Trades); err != nil {
		return nil, err
	}
	trades := make([]models.HistoricalTrade, 0)
	for _, tr := range Trades {

		trade := models.HistoricalTrade{
			ID:           tr.ID,
			Name:         pair,
			Price:        tr.Price,
			Qty:          tr.Qty,
			QuoteQty:     tr.QuoteQty,
			Time:         tr.Time,
			IsBuyerMaker: tr.IsBuyerMaker,
			IsBestMatch:  tr.IsBestMatch,
		}
		trades = append(trades, trade)
	}
	return trades, nil
}

// Получаем список всех торгуемых пар с Binance
func (w *Worker) fetchAllPairs() ([]string, error) {
	var info models.ExchangeInfo
	// запрос к binance для получения информации по парам
	if err := w.makeAPIRequest(ExchangeInfoAPIURL, &info); err != nil {
		return nil, err
	}

	// формируем список пар для возврата
	pairs := make([]string, 0, len(info.Symbols))
	for _, symbol := range info.Symbols {
		pairs = append(pairs, symbol.Symbol)
	}
	return pairs, nil
}

// общая функция отправки запроса к апи и декодирования ответа
func (w *Worker) makeAPIRequest(url string, result interface{}) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	req.Header.Add("X-MBX-APIKEY", w.ApiKey) // Добавление ключа API в заголовки

	// отправка запроса
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close() // Закрытие тела ответа после завершения функции

	// проверка статуса ответа
	if res.StatusCode != 200 {
		return fmt.Errorf("error response from Binance API: %s", res.Status)
	}

	// декодирование ответа
	return json.NewDecoder(res.Body).Decode(result)
}
