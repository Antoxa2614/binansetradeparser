package storage

import (
	"context"
	"gitlab.com/antoxa2614/binansetradeparser/internal/models"
	"gitlab.com/golight/orm/db/adapter"
	"gitlab.com/golight/orm/utils"
	"go.uber.org/zap"
)

type Storage struct {
	adapter *adapter.SQLAdapter
	logger  *zap.Logger
}

func NewStorage(adapter *adapter.SQLAdapter, logger *zap.Logger) *Storage {
	return &Storage{adapter: adapter, logger: logger}
}

func (s Storage) Save(ctx context.Context, historicalTrades []models.HistoricalTrade) error {
	for _, trade := range historicalTrades {

		tradeDTO := models.HistoricalTradeDTO{
			ID:           trade.ID,
			Price:        trade.Price,
			Qty:          trade.Qty,
			QuoteQty:     trade.QuoteQty,
			Time:         trade.Time,
			IsBuyerMaker: trade.IsBuyerMaker,
			IsBestMatch:  trade.IsBestMatch,
		}

		err := s.adapter.Create(ctx, &tradeDTO)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Storage) GetInfo(symbol string) ([]models.HistoricalTradeDTO, error) {
	// Инициализируем условие для выборки записей по символу
	condition := utils.Condition{
		Equal: map[string]interface{}{
			"name": symbol,
		},
	}
	// инициализируем массив для результатов
	var trades []models.HistoricalTradeDTO

	// выполняем запрос к базе данных
	err := s.adapter.List(context.TODO(), &trades, "trades", condition)
	if err != nil {
		return nil, err
	}

	if len(trades) == 0 {
		s.logger.Info("no any data", zap.String("symbol", symbol))
	}
	return trades, nil
}
