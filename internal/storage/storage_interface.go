package storage

import (
	"context"
	"gitlab.com/antoxa2614/binansetradeparser/internal/models"
)

type Storager interface {
	Save(ctx context.Context, klines []models.HistoricalTrade) error
	GetInfo(symbol string) ([]models.HistoricalTradeDTO, error)
}
