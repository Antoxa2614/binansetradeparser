package service

import (
	"context"
	"gitlab.com/antoxa2614/binansetradeparser/internal/models"
)

type Servicer interface {
	StartParsing(ctx context.Context)
	StopParsing()
	GetInfo(symbol string) ([]models.HistoricalTradeDTO, error)
}
