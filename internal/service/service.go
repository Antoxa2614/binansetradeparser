package service

import (
	"context"
	"gitlab.com/antoxa2614/binansetradeparser/internal/models"
	"gitlab.com/antoxa2614/binansetradeparser/internal/storage"
	"gitlab.com/antoxa2614/binansetradeparser/internal/worker"
)

type Service struct {
	Storage storage.Storager
	Worker  *worker.Worker
}

func NewService(storage *storage.Storage, worker *worker.Worker) *Service {
	return &Service{Storage: storage, Worker: worker}
}

func (s *Service) StartParsing(ctx context.Context) {
	if s.Worker.IsRunning {
		return
	}
	s.Worker.IsRunning = true
	s.Worker.StopChan = make(chan bool)

	go s.Worker.Run(ctx, s.Worker.StopChan)
}

func (s *Service) StopParsing() {
	if !s.Worker.IsRunning {
		return
	}
	s.Worker.StopChan <- true
	close(s.Worker.StopChan)

	s.Worker.IsRunning = false
}

func (s *Service) GetInfo(symbol string) ([]models.HistoricalTradeDTO, error) {
	return s.Storage.GetInfo(symbol)
}
