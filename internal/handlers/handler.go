package handler

import (
	"context"
	"encoding/json"
	"gitlab.com/antoxa2614/binansetradeparser/internal/service"
	"go.uber.org/zap"
	"net/http"
)

type Handler struct {
	Service service.Servicer
	logger  *zap.Logger
}

func NewHandler(service service.Servicer, logger *zap.Logger) *Handler {
	return &Handler{Service: service, logger: logger}
}

func (h *Handler) StartParsing(w http.ResponseWriter, r *http.Request) {
	h.Service.StartParsing(context.Background())
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Parsing started"))
}

func (h *Handler) StopParsing(w http.ResponseWriter, r *http.Request) {
	h.Service.StopParsing()
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Parsing stopped"))
}

func (h *Handler) GetInfo(w http.ResponseWriter, r *http.Request) {
	var requestData struct {
		Symbol string `json:"symbol"`
	}

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.logger.Error("symbol decode error", zap.Error(err))
		return
	}

	klines, err := h.Service.GetInfo(requestData.Symbol)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.logger.Error("getinfo error", zap.Error(err))
		return
	}

	if err = json.NewEncoder(w).Encode(klines); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		h.logger.Error("encode error", zap.Error(err))
		return
	}

}
