package handler

import "github.com/go-chi/chi/v5"

func (h *Handler) InitRoutes() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/start", h.StartParsing)
	r.Post("/stop", h.StopParsing)
	r.Get("/getinfo", h.GetInfo)
	return r
}
