package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/antoxa2614/binansetradeparser/config"
	"gitlab.com/antoxa2614/binansetradeparser/run"
	"go.uber.org/zap"
	"os"
)

func main() {
	err := godotenv.Load()
	conf := config.NewAppConf()
	logger, err := zap.NewDevelopment()
	if err != nil {
		fmt.Printf("Cannot initialize zap logger: %v", err)
		os.Exit(1)
	}
	defer logger.Sync() // очистка логгера перед завершением программы
	if err != nil {

	}
	app := run.NewApp(conf, logger)
	// инициализируем и запуcкаем приложение
	exitCode := app.Bootstrap().Run()
	os.Exit(exitCode)
}
